# Createsa Docker image for running springboot application

# Adapted from https://about.gitlab.com/2016/12/14/continuous-delivery-of-a-spring-boot-application-with-gitlab-ci-and-kubernetes/
FROM openjdk:8-jre
VOLUME /tmp
ADD /target/springboot-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
