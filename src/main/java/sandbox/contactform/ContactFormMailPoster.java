/**
 * 
 */
package sandbox.contactform;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


/**
 * Contact Form Poster that send an email with Information found in environment variable.
 * @author ynicolas
 *
 */
@Service
public class ContactFormMailPoster implements ContactFormPoster {

	
	@Value("${smtp.host}")
	private String mailHost;
	
	@Value("${smtp.user}")
	private String mailUser;
	
	@Value("${smtp.pwd}")
	private String mailPwd;

	@Value("${smtp.recipient}")
	private String mailRecipient;
	
	private final String subject = "[MON SITE] contact";

	private Email emailFactory() throws EmailException{
		Email toReturn = new SimpleEmail();
		toReturn.setHostName(mailHost);
		toReturn.setSmtpPort(465);
		toReturn.setAuthenticator(new DefaultAuthenticator(mailUser, mailPwd));
		toReturn.setSSLOnConnect(true);
		toReturn.addTo(mailRecipient);
		toReturn.setSubject(subject);
		return toReturn;
	}
	
	/* (non-Javadoc)
	 * @see ContactForm.ContactFormPoster#send(java.lang.String, java.lang.String)
	 */
	@Override
	public void send(String sender, String Information) throws Exception {
		Email toSend = this.emailFactory();
		toSend.setFrom(sender);
		toSend.setMsg(Information);
		toSend.send();

	}

}
