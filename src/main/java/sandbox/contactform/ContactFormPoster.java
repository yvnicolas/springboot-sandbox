/**
 * 
 */
package sandbox.contactform;

/**
 * @author ynicolas
 *
 */
public interface ContactFormPoster {
	
	public void send(String sender, String Information) throws Exception;

}
