package sandbox.controllers;

import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import sandbox.contactform.ContactFormPoster;

@RestController
public class MainController {

    private static final String template = "Bienvenue sur serveur mail, %s!";
    private static final String defaultSender = "yves.nicolas@yvni.com";
    
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping(path="/", method=RequestMethod.GET)
    public Greeting greeting(@RequestParam(value="name", defaultValue="Le monde") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }
    
    
    @Autowired
    ContactFormPoster mailPoster;
    @RequestMapping(path="/", method=RequestMethod.POST)
    public String sendMail(@RequestParam(value="sender", defaultValue=defaultSender) String sender,
    		@RequestParam(value="information", defaultValue="Bonjour") String info,
    		HttpServletResponse response) {
    	
    	try {
    	mailPoster.send(sender, info);
    	return "OK";
    	} catch (Exception e) {
    		throw new HttpMessageNotWritableException("Send Mail KO : "+e.getMessage(), e);
    	}
    	
    }
    
    
    
    
}
