package sandbox.contactform;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import sandbox.contactform.ContactFormMailPoster;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ContactFormMailPosterTest {
	
	@Autowired
	private ContactFormMailPoster underTest;

	@Test
	public void testInitEnvironment() {
		
		assertNotNull(underTest);
	}
	
	// To be "uningnored" if you want to send real mail with environment variable setup
	@Test
	@Ignore
	public void testSend() throws Exception {
		underTest.send("yves.nicolas@sender.com", "bonjour");
	}
	
	@Test
	public void testWrongSender() {
		try {
			underTest.send("Yves", "bonjour");
			fail ("Should raise Exception : wrong sender");
		} catch (Exception e) {
			// valid behaviour, do nothing
		}

	}

}
