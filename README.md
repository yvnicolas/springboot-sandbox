A Springboot based micro services project to send by mail the content of a web form.

This project is both used to sandbox use of gitlab based CI features

It tests and build jar with maven for every branch and produces docker images when 
merging with master and deploying them to GKE when pushing tags.
